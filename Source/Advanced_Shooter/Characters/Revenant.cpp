// Fill out your copyright notice in the Description page of Project Settings.

#include "Revenant.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


// Sets default values
ARevenant::ARevenant()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	// create camera boom object on the heap.
	// basics are set here but advanced options can be tweeked in bp as well. (like CollisionTests and Lags...)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent, NAME_None);
	CameraBoom->TargetArmLength = 300.f;
	// enable rotation of camera boom based on the controllers rotation. (note: controllers have rotation but no location)	 
	CameraBoom->bUsePawnControlRotation = true;


	// create follow camera object on the heap.
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	// attach the camera to the end of the camera boom.
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// camera is not responsible for handling the rotation input from the controller.
	FollowCamera->bUsePawnControlRotation = false;
}

// Called when the game starts or when spawned
void ARevenant::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void ARevenant::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARevenant::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	// bind axes
	PlayerInputComponent->BindAxis("MoveForward", this, &ARevenant::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARevenant::MoveRight);


}

void ARevenant::MoveForward(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		// find out the forward direction (based on the controller's rotation)
		const FRotator Rotation{ Controller->GetControlRotation() };
		const FRotator YawRotation{ 0.f, Rotation.Yaw, 0.f };

		// get a unit vector of the direction based on the X axis
		// e.g, if controller is facing the x axis direction is (1.f,0.f,0.f)
		// if it's facing the y axis (0.f, 0.f, 0.f)
		const FVector Direction{ FRotationMatrix{YawRotation}.GetUnitAxis(EAxis::X) };

		// now pass that unit vector into movement component(not directly)
		AddMovementInput(Direction, Value);
	}
}

void ARevenant::MoveRight(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		// find out the forward direction (based on the controller's rotation)
		const FRotator Rotation{ Controller->GetControlRotation() };
		const FRotator YawRotation{ 0.f, Rotation.Yaw, 0.f };

		// get a unit vector of the direction based on the Y axis
		// e.g, if controller is facing the x axis direction is (0.f,0.f,0.f)
		// if it's facing the y axis (0.f, 1.f, 0.f)
		const FVector Direction{ FRotationMatrix{YawRotation}.GetUnitAxis(EAxis::Y) };

		// now pass that unit vector into movement component(not directly)
		AddMovementInput(Direction, Value);
	}
}
