// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Revenant.generated.h"

UCLASS()
class ADVANCED_SHOOTER_API ARevenant : public ACharacter
{
	GENERATED_BODY()

////////////////////////////////////////////////////////////////////////// member variables
public:
	
protected:

private:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom{ nullptr };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera{ nullptr };

////////////////////////////////////////////////////////////////////////// member functions
public:
	// Sets default values for this character's properties
	ARevenant();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Value);
	void MoveRight(float Value);


public:
////////////////////////////////////////////////////////////////////////// INLINE functions

	// returns a reference to CameraBoom object 
	FORCEINLINE USpringArmComponent* GetCameraBoom() const
	{
		return CameraBoom;
	}

	// returns a reference to CameraBoom object 
	FORCEINLINE UCameraComponent* GetFollowCamera() const
	{
		return FollowCamera;
	}
};
