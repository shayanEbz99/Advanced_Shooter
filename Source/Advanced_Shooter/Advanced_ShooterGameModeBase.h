// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Advanced_ShooterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ADVANCED_SHOOTER_API AAdvanced_ShooterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
