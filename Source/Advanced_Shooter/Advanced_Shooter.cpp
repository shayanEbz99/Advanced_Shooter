// Copyright Epic Games, Inc. All Rights Reserved.

#include "Advanced_Shooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Advanced_Shooter, "Advanced_Shooter" );
